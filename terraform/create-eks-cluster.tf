variable "env" {}
variable "instance_types" {}
variable "app_name" {}

data "aws_caller_identity" "current" {}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.17.2"

  cluster_name                   = var.cluster_name
  cluster_version                = "1.28"
  cluster_endpoint_public_access = true

  subnet_ids = module.vpc.private_subnets
  vpc_id     = module.vpc.vpc_id
  tags = {
    environment = "development"
    application = var.app_name
  }

  enable_irsa = true

  cluster_addons = {
    coredns = {
      most_recent                 = true
      resolve_conflicts_on_update = false
      resolve_conflicts_on_create = true
    }
    kube-proxy = {
      most_recent                 = true
      resolve_conflicts_on_update = false
      resolve_conflicts_on_create = true
    }
    vpc-cni = {
      most_recent                 = true
      resolve_conflicts_on_update = false
      resolve_conflicts_on_create = true
    }

    # starting from EKS 1.23 CSI plugin is needed for volume provisioning.
    # https://stackoverflow.com/questions/75758115/persistentvolumeclaim-is-stuck-waiting-for-a-volume-to-be-created-either-by-ex
    aws-ebs-csi-driver = {
      service_account_role_arn    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.cluster_name}-ebs-csi-controller"
      resolve_conflicts_on_update = false
      resolve_conflicts_on_create = true
    }
  }

  # worker nodes
  eks_managed_node_groups = {
    nodegroup = {
      use_custom_templates = false
      instance_types       = [var.instance_types]
      node_group_name      = "${var.cluster_name}-ng"

      desired_size = 2
      min_size     = 2
      max_size     = 3

      labels = {
        Production = "ng"
      }
      tags = {
        Name = "${var.env}"
        node = "eks-ng"
      }
    }
  }

  # fargate_profiles = {
  #   profile = {
  #     name = "fargate"
  #     selectors = [
  #       {
  #         namespace = "fargate"
  #         labels = {
  #           Production = "fargate"
  #         }
  #       }
  #     ]
  #     tags = {
  #       Name = "${var.env}"
  #     }
  #   }
  # }
}

# starting from EKS 1.23 CSI plugin is needed for volume provisioning.
# https://stackoverflow.com/questions/75758115/persistentvolumeclaim-is-stuck-waiting-for-a-volume-to-be-created-either-by-ex
