terraform {
  required_version = ">= 1.6.1"
  backend "s3" {
    bucket = "twwn-tfstate"
    key    = "chapter11-exercise/state.tfstate"
    region = "ap-southeast-1"
  }
}
